/*


Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include <SPI.h>
#include "constants.h"
#include <EEPROM.h>
#include "boards.h"
#include <RBL_nRF8001.h>
#include <IRremote.h>
#include <LiquidCrystal.h>
#include <SimpleTimer.h>

// ========== BRUNOTV VERSION ========== //

#define PROTOCOL_MAJOR_VERSION   0 //
#define PROTOCOL_MINOR_VERSION   0 //
#define PROTOCOL_BUGFIX_VERSION  2 // bugfix

#define PIN_CAPABILITY_NONE      0x00
#define PIN_CAPABILITY_DIGITAL   0x01
#define PIN_CAPABILITY_ANALOG    0x02
#define PIN_CAPABILITY_I2C       0x10
#define ANALOG                   0x02 // analog pin in analogInput mode

byte pin_mode[TOTAL_PINS];
byte pin_state[TOTAL_PINS];
byte pin_pwm[TOTAL_PINS];
//byte pin_servo[TOTAL_PINS];

float lowMoneyLimit = 1.50f;

//LCD
LiquidCrystal lcd(12,11,5,4,3,2);
const String lcdWelcomeLine1 = "BTV(tm) listo";
const String lcdWelcomeLine2 = "Esperando enlace";
bool lcdDeviceConnectedMessageVisible = false;
bool lcdWelcomeMessageVisible = false;
bool lcdBalanceMessageVisible = false;
LCDVisibleMessage currentVisibleLCDMessage;

// TIMER
SimpleTimer timer;

int    minutesLeftTimerID, shutdownCounterTimerID;
bool   minutesLeftTimerActive = false;
bool   shutDownTimerActive = false;
float  minutesRemaining;

//Infrared
IRsend irsend;
IRrecv irrecv(10);
int PIN_BLE_CONNECTED = 6;
int PIN_TV_STATUS = 7;

// Buttons
unsigned int power[67];
unsigned int volumeUp[67];
unsigned int volumeDown[67];
unsigned int programUp[67];
unsigned int programDown[67];
unsigned int guide[67];
unsigned int cursorUp[67];
unsigned int cursorDown[67];
unsigned int enter_ok[67];

int shutdownCounter = 0.0;

//setup
int enteredcode = -1;
bool setupCompleted;
int buttonState = 0;
decode_results results;
int codeType = -1; // The type of code

//Servo servos[MAX_SERVOS];


/////////////////////////// S E T U P ///////////////////////////////

void setup()
{
  
  minutesRemaining = 20;
  shutdownCounter = 10000;
  lcd.begin(16,2);
  pinMode(13,INPUT); //restart button

  EEPROM_readAnything(0,setupCompleted);

  Serial.begin(57600);
  
  Serial.println("BLE Arduino Slave");
  
  minutesLeftTimerID = timer.setInterval(1000, minutesLeftTimerf);
    
  shutdownCounterTimerID = timer.setInterval(1000, shutdownCounterTimerf);
  
  pinMode(PIN_BLE_CONNECTED, OUTPUT);
  
  pinMode(PIN_TV_STATUS,OUTPUT);
  
  //disable both timers
  timer.disable(minutesLeftTimerID);
  timer.disable(shutdownCounterTimerID);
  
  if (!setupCompleted) 
  {
      //clear EPROM
    for (int i = 0; i < 512; i++)
      EEPROM.write(i, 0);
      
    lcd.print("Configuracion");
    lcd.setCursor(0,1);
    lcd.print("inicial");
    delay(2000);
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Pulsa 1 vez la ");
    lcd.setCursor(0,1);
    lcd.print("tecla indicada");
    delay(4000);
    lcd.clear();
    lcd.print("Boton encendido");
    irrecv.enableIRIn(); // Start the receiver
  }
  else
  {
    lcd.clear();
    
    printLcdWelcomeMessage();
    //lcd.print("Bienvenido");
    int mempos;
    
    mempos = sizeof(setupCompleted) +1;
    
    EEPROM_readAnything( mempos, power);

    mempos = ( sizeof(setupCompleted) + sizeof(power) ) +1;
   
    EEPROM_readAnything( mempos , guide);
    
    mempos = ( sizeof(setupCompleted) + sizeof(power)  + sizeof(guide) ) + 1 ;

    EEPROM_readAnything(mempos, volumeUp);

    mempos = ( sizeof(setupCompleted) + sizeof(power) + sizeof(guide) +  sizeof(volumeUp) ) + 1;
    
    EEPROM_readAnything(mempos, volumeDown);
    
    mempos = ( sizeof(setupCompleted) + sizeof(power)  +  sizeof(guide) +  sizeof(volumeUp) +  sizeof(volumeDown) ) + 1;

    EEPROM_readAnything(mempos, cursorUp);
    
    mempos = ( sizeof(setupCompleted) + sizeof(power)  +  sizeof(guide) +  sizeof(volumeUp) +  sizeof(volumeDown) +  sizeof(cursorUp) ) + 1;

    EEPROM_readAnything(mempos, cursorDown);
    
    mempos = ( sizeof(setupCompleted) + sizeof(power)  +  sizeof(guide) +  sizeof(volumeUp) +  sizeof(volumeDown) +  sizeof(cursorUp) +  sizeof(cursorDown) ) + 1;
   
    EEPROM_readAnything(mempos, enter_ok);

    // Set your BLE Shield name here, max. length 10
    ble_set_name("BrunoTV");
    // Init. and start BLE library.
    ble_begin();
  }
  

}

/////////////////////////////////////////////////////////////////////////////

static byte buf_len = 0;

///////////////////////////////// FUNCTIONS ///////////////////////////////// 


void minutesLeftTimerf() {
  if (minutesRemaining <= lowMoneyLimit) 
  {
    minutesLeftTimerActive = false;
    timer.disable(minutesLeftTimerID);
    //timer.enable(shutdownTimerID); // executed once after 10 seconds
    lcd.clear();
    lcd.print("Saldo agotado");
    timer.restartTimer(shutdownCounterTimerID);
    timer.enable(shutdownCounterTimerID); //executed withing 10 seconds prior shutdown to inform user
  }
  else
  {
    lcd.setCursor(0,1);
    minutesRemaining  = minutesRemaining -  0.5f;
    lcd.print(minutesRemaining);
    lcd.print(" minutos");
  }
}

void shutdownCounterTimerf()
{
    if (shutdownCounter == 10000) lcd.clear();
    lcd.setCursor(0,1);
    lcd.print("Apagado en");
    lcd.setCursor(11,1);
    lcd.print( (int) shutdownCounter/1000);
    if (shutdownCounter == 0)
    {
        lcd.clear();
        timer.disable(shutdownCounterTimerID);
        digitalWrite(7,LOW);
        irsend.sendRaw(power,68,38);
        lcd.setCursor(0,1);
        lcdPrintScrolling("No dispone de suficiente saldo, para continuar recargue a traves de su smartphone"," TV Apagado");
    }
    shutdownCounter = shutdownCounter - 1000;
}


void activeLCDMessage(enum LCDVisibleMessage t)
{
  if ( timer.isEnabled(minutesLeftTimerID) ) return;
  
  switch (t){
    case LCDVisibleMessageWelcome:{
      if (!lcdWelcomeMessageVisible)
      {
        lcd.clear();
        lcd.setCursor (2,0);
        lcd.print(lcdWelcomeLine1);
        lcd.setCursor (0,1);
        lcd.print(lcdWelcomeLine2);
        lcdWelcomeMessageVisible = true;
        lcdDeviceConnectedMessageVisible = false;
      }
    }
    break;
    case LCDVisibleMessageDeviceConnected:{
      if (!lcdDeviceConnectedMessageVisible)
      {
        lcd.clear();
        lcd.print("Conectado a ");
        lcd.setCursor(0,1);
        lcd.print("test");
        lcdDeviceConnectedMessageVisible = true;
        lcdWelcomeMessageVisible = false;
      }
    }
    break;
  }
    currentVisibleLCDMessage = t;
}



void lcdPrintScrolling(String line1, String line2){
  int screenWidth = 16;
  int screenHeight = 2;
  int scrollCursor = screenWidth;
  int stringStart, stringStop = 0;
  int counter = 0;
  
 while (counter < line1.length()){ 
  lcd.setCursor(scrollCursor, 0);
  lcd.print(line1.substring(stringStart,stringStop));
  lcd.setCursor(0, 1);
  lcd.print(line2);
  delay(300);
  lcd.clear();
  if(stringStart == 0 && scrollCursor > 0){
    scrollCursor--;
    stringStop++;
  } else if (stringStart == stringStop){
    stringStart = stringStop = 0;
    scrollCursor = screenWidth;
  } else if (stringStop == line1.length() && scrollCursor == 0) {
    stringStart++;
  } else {
    stringStart++;
    stringStop++;
  }
  counter ++;
 }
 activeLCDMessage(currentVisibleLCDMessage);
}

void printLcdWelcomeMessage()
{
  lcd.setCursor (2,0);
  lcd.print(lcdWelcomeLine1);
  lcd.setCursor (0,1);
  lcd.print(lcdWelcomeLine2);
  lcdWelcomeMessageVisible = true;
}

void printLCDConnectedTo(String device)
{
  lcd.clear();
  lcd.print("Conectado a ");
  lcd.setCursor(0,1);
  lcd.print(device);
}


void ble_write_string(byte *bytes, uint8_t len)
{
  if (buf_len + len > 20)
  {
    for (int j = 0; j < 15000; j++)
      ble_do_events();
    
    buf_len = 0;
  }
  
  for (int j = 0; j < len; j++)
  {
    ble_write(bytes[j]);
    buf_len++;
  }
    
  if (buf_len == 20)
  {
    for (int j = 0; j < 15000; j++)
      ble_do_events();
    
    buf_len = 0;
  }  
}


byte reportDigitalInput()
{
  if (!ble_connected())
    return 0;

  static byte pin = 0;
  byte report = 0;
  
  if (!IS_PIN_DIGITAL(pin))
  {
    pin++;
    if (pin >= TOTAL_PINS)
      pin = 0;
    return 0;
  }
  
  if (pin_mode[pin] == INPUT)
  {
      byte current_state = digitalRead(pin);
            
      if (pin_state[pin] != current_state)
      {
        pin_state[pin] = current_state;
        byte buf[] = {'G', pin, INPUT, current_state};
        ble_write_string(buf, 4);
        
        report = 1;
      }
  }
  
  pin++;
  if (pin >= TOTAL_PINS)
    pin = 0;
    
  return report;
}


byte reportPinAnalogData()
{
  if (!ble_connected())
    return 0;
    
  static byte pin = 0;
  byte report = 0;
  
  if (!IS_PIN_DIGITAL(pin))
  {
    pin++;
    if (pin >= TOTAL_PINS)
      pin = 0;
    return 0;
  }
  
  if (pin_mode[pin] == ANALOG)
  {
    uint16_t value = analogRead(pin);
    byte value_lo = value;
    byte value_hi = value>>8;
    
    byte mode = pin_mode[pin];
    mode = (value_hi << 4) | mode;
    
    byte buf[] = {'G', pin, mode, value_lo};         
    ble_write_string(buf, 4);
  }
  
  pin++;
  if (pin >= TOTAL_PINS)
    pin = 0;
    
  return report;
}

void reportPinDigitalData(byte pin)
{
  byte state = digitalRead(pin);
  byte mode = pin_mode[pin];
  byte buf[] = {'G', pin, mode, state};         
  ble_write_string(buf, 4);
}


void sendCustomData(uint8_t *buf, uint8_t len)
{
  uint8_t data[20] = "Z";
  memcpy(&data[1], buf, len);
  ble_write_string(data, len+1);
}

byte queryDone = false;



void software_Reset() // Restarts program from beginning but does not reset the peripherals and registers
{
  asm volatile ("  jmp 0");  
} 


void storeCode(decode_results *results, int memPos) {
  unsigned int rawCodes[RAWBUF]; // The durations if raw
  codeType = results->decode_type;
  int count = results->rawlen;
  int codeLen = results->rawlen - 1;
    for (int i = 1; i <= codeLen; i++) {
      if (i % 2) {
        // Mark
        rawCodes[i - 1] = results->rawbuf[i]*USECPERTICK - MARK_EXCESS;
        //Serial.print(" m");
      } 
      else {
        // Space
        rawCodes[i - 1] = results->rawbuf[i]*USECPERTICK + MARK_EXCESS;
        //Serial.print(" s");
      }
      //Serial.print(rawCodes[i - 1], DEC);
    }
    //Serial.println();
      //  Serial.print(codeLen);

    //Serial.println(results->value, HEX);
    //codeValue = results->value;
    //codeLen = results->bits;
    EEPROM_writeAnything(memPos, rawCodes);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// MAIN LOOP ///////////////////////
////////////////////////////////////////////////////////////////////////////////

void loop()
{
    timer.run();
  
    buttonState = digitalRead(13);
    if (buttonState == HIGH) {
          lcd.clear();
          lcd.print("Reiniciando ...");
          setupCompleted = false;
          EEPROM_writeAnything(0,setupCompleted);
          delay(1000);
          software_Reset();
    }
    
    
  while (!setupCompleted)
  {
    if (irrecv.decode(&results)) // if code can be decoded store it
    {
      enteredcode ++;

      switch (enteredcode)
      {
        case 0: //////////////////// Store Power ///////////////
        {
         /// guardar codigo para boton encendido 
                 //Serial.println(results.value, HEX);
          //dump(&results);
          int mempos = sizeof(setupCompleted) + 1;
          storeCode(&results,0);
          EEPROM_readAnything(0, power); 
          irrecv.resume(); // Receive the next value
          lcd.clear();
          lcd.print("Boton GUIDE"); // siguiente
        }
        break;
        
        case 1:  //////////////////// Store guide  ///////////////
        {
          int mempos = (sizeof(setupCompleted) + sizeof(power))+ 1;
          storeCode(&results, mempos);
          EEPROM_readAnything(mempos, guide);
          irrecv.resume(); // Receive the next value
          lcd.clear();
          lcd.print("Bot. Volumen +"); // siguiente
        }
        break;
        
        case 2:  //////////////////// Store volume +  ///////////////
        {
          int mempos = ( sizeof(setupCompleted) + sizeof(power)  + sizeof(guide) ) + 1 ;
          storeCode(&results, mempos);
          EEPROM_readAnything(mempos, volumeUp);
          irrecv.resume(); // Receive the next value
          lcd.clear();
          lcd.print("Bot. Volumen -"); // siguiente
        }
        break;
        
        case 3:  //////////////////// Store volume -  ///////////////
        {
          int mempos = ( sizeof(setupCompleted) + sizeof(power) + sizeof(guide) +  sizeof(volumeUp) ) + 1;
          storeCode(&results, mempos);
          EEPROM_readAnything(mempos, volumeDown);
          irrecv.resume(); // Receive the next value
          lcd.clear();
          lcd.print("Flecha arriba"); // siguiente
        }
        break;
        
        case 4:  //////////////////// Store cursor up  ///////////////
        {
          int mempos = ( sizeof(setupCompleted) + sizeof(power)  +  sizeof(guide) +  sizeof(volumeUp) +  sizeof(volumeDown) ) + 1;
          storeCode(&results, mempos);
          EEPROM_readAnything(mempos, cursorUp);
          irrecv.resume(); // Receive the next value
          lcd.clear();
          lcd.print("Flecha abajo"); // siguiente
        }
        break;
        
        case 5:  //////////////////// Store cursor down  ///////////////
        {
          int mempos = ( sizeof(setupCompleted) + sizeof(power)  +  sizeof(guide) +  sizeof(volumeUp) +  sizeof(volumeDown) +  sizeof(cursorUp) ) + 1;
          storeCode(&results, mempos);
          EEPROM_readAnything(mempos, cursorDown);
          irrecv.resume(); // Receive the next value
          lcd.clear();
          lcd.print("Ok o enter"); // siguiente
        }
        break;
        
        case 6: //////////////////// Store ok enter  ///////////////
        {
          int mempos = ( sizeof(setupCompleted) + sizeof(power)  +  sizeof(guide) +  sizeof(volumeUp) +  sizeof(volumeDown) +  sizeof(cursorUp) +  sizeof(cursorDown) ) + 1;
          storeCode(&results, mempos);
          EEPROM_readAnything(mempos, enter_ok);
          lcd.clear();
          lcd.print("Completado"); // siguiente
          setupCompleted = true;
          EEPROM_writeAnything(0,setupCompleted);
          
             // Set your BLE Shield name here, max. length 10
            ble_set_name("BrunoTV");
            // Init. and start BLE library.
            ble_begin();
        }
        break;
      }

    }

     Serial.println(enteredcode);
  } // while
  


  while(ble_available())
  {
    byte cmd;
    cmd = ble_read();
    Serial.write(cmd);
    // Parse data here
    switch (cmd)
    {
      case 'V': // query protocol version
        {
          byte buf[] = {'V', 0x00, 0x00, 0x01};
          ble_write_string(buf, 4);
        }
        break;
      
      case 'C': // query board total pin count
        {
          byte buf[2];
          buf[0] = 'C';
          buf[1] = TOTAL_PINS; 
          ble_write_string(buf, 2);
        }        
        break;
      
      case 'M': // query pin mode
        {  
          byte pin = ble_read();
          byte buf[] = {'M', pin, pin_mode[pin]}; // report pin mode
          ble_write_string(buf, 3);
        }  
        break;
      
      case 'S': // set pin mode
        {
          byte pin = ble_read();
          byte mode = ble_read();
          /* ToDo: check the mode is in its capability or not */
          /* assume always ok */
          if (mode != pin_mode[pin])
          {              
            pinMode(pin, mode);
            pin_mode[pin] = mode;
          
            if (mode == OUTPUT)
            {
              digitalWrite(pin, LOW);
              pin_state[pin] = LOW;
            }
            else if (mode == INPUT)
            {
              digitalWrite(pin, HIGH);
              pin_state[pin] = HIGH;
            }
            else if (mode == ANALOG)
            {
              if (IS_PIN_ANALOG(pin)) {
                if (IS_PIN_DIGITAL(pin)) {
                  pinMode(PIN_TO_DIGITAL(pin), LOW);
                }
              }
            }
          }
            
          if ( (mode == INPUT) || (mode == OUTPUT) )
            reportPinDigitalData(pin);
          //else if (mode == PWM)
            //reportPinPWMData(pin);
        }
        break;

      case 'G': // query pin data
        {
          byte pin = ble_read();
          reportPinDigitalData(pin);
        }
        break;
        
      case 'T': // set pin digital state
        {
          byte pin = ble_read();
          byte state = ble_read();
          
          digitalWrite(pin, state);
          reportPinDigitalData(pin);
        }
        break;
      /*case 'A': // query all pin status
        for (int pin = 0; pin < TOTAL_PINS; pin++)
        {
          reportPinCapability(pin);
          if ( (pin_mode[pin] == INPUT) || (pin_mode[pin] == OUTPUT) )
            reportPinDigitalData(pin);
          else if (pin_mode[pin] == PWM)
            reportPinPWMData(pin);
        }
        
        queryDone = true; 
        {
          uint8_t str[] = "ABC";
          sendCustomData(str, 3);
        }
       
        break;*/
          
      case 'P': // query pin capability
        {
          byte pin = ble_read();
          //reportPinCapability(pin);
        }
        break;
        
      case 'Z':
        {
          byte len = ble_read();
          byte buf[len];
          for (int i=0;i<len;i++)
            buf[i] = ble_read();
           
          for (int i=0;i<len;i++)
           Serial.print(buf[i]); 
           Serial.println();
           Serial.print(buf[2]);
           //Serial.print(buf[2]); 
          if (buf[2] == 80){  /////////////////////////////////////////////// VOLUME UP ///////////////////////////////
                irsend.sendRaw(volumeUp, 68, 38);
          }
          else if (buf[2] == 82){  /////////////////////////////////////////////// P O W E R ////////////////
            if (minutesRemaining > lowMoneyLimit)
            {
                lcd.clear();
                irsend.sendRaw(power,68,38); 
                int tvPower = digitalRead(7);
                if (tvPower)
                {
                   digitalWrite(7,LOW);
                   timer.disable(minutesLeftTimerID);
                   //minutesLeftTimerActive = false;
                   //timer.disable(minutesLeftTimerID);
                }
                 else
                 {
                   lcd.clear();
                   digitalWrite(7,HIGH);
                   timer.enable(minutesLeftTimerID);
                   lcd.print("Tiempo restante");
                   lcdDeviceConnectedMessageVisible = false;
                   lcdWelcomeMessageVisible = false;
                 }
            }
            else
            {
              lcd.clear();
              lcd.print("Saldo insuficiente");
            }
          }
          else if(buf[2] == 77) /////////////////////////////////////////////// CURSOR UP ///////////////////////////////
          {
             irsend.sendRaw(cursorUp,68,38);
          }
          else if(buf[2] == 81) /////////////////////////////////////////////// CURSOR DOWN ///////////////////////////////
          {
             irsend.sendRaw(cursorDown,68,38);
          }
          else if(buf[2] == 72) /////////////////////////////////////////////// GUIDE ///////////////////////////////
          {
             irsend.sendRaw(guide,68,38);
          }
          else if(buf[2] == 84) /////////////////////////////////////////////// ENTER OK ///////////////////////////////
          {
             irsend.sendRaw(enter_ok,68,38);
          }
          else if (buf[2] == 78){ /////////////////////////////////////////////// VOLUME DOWN ///////////////////////////////
            irsend.sendRaw(volumeDown,68,38);
          }
          else if (buf[2] == 85){
            if (minutesRemaining > lowMoneyLimit)
              {
                irsend.sendRaw(programUp,68,38);
                int tvPower = digitalRead(7);
                if (!tvPower)
                   digitalWrite(7,HIGH);
              }
          }
          else if (buf[2] == 68){
           if (minutesRemaining > lowMoneyLimit)
             {
              irsend.sendRaw(programDown,68,38);
              int tvPower = digitalRead(7);
              uint8_t str[] = "2.50";
              sendCustomData(str, 3);
              if (!tvPower)
                 digitalWrite(7,HIGH);
             }
                 
          }
          else if (buf[0] == 49)  /////  saldo de 10 horas
          {
            minutesRemaining = minutesRemaining + 50;
            lcd.clear();
            lcd.print("Recargado 10h!");
            delay(3000);
            lcd.clear();
            activeLCDMessage(currentVisibleLCDMessage);
          }
      }
    }

    // send out any outstanding data 
    ble_do_events();
    buf_len = 0;
    
    return; // only do this task in this loop
  }

  // process text data
  if (Serial.available())
  {
    byte d = 'Z';
    ble_write(d);

    delay(5);
    while(Serial.available())
    {
      d = Serial.read();
      ble_write(d);
    }
    
    ble_do_events();
    buf_len = 0;
    
    return;    
  }

  /////////////////////////////////////// BLE Conected / Disconnected status
  if (ble_connected())
  {
     activeLCDMessage(LCDVisibleMessageDeviceConnected);
     digitalWrite(6,HIGH);
  }
  else if (!ble_connected())
  {
     activeLCDMessage(LCDVisibleMessageWelcome);
     digitalWrite(6,LOW);
     int tvPower = digitalRead(7);
     if (tvPower){
         digitalWrite(7,LOW);
         irsend.sendRaw(power,68,38);
     }
   }
   ////////////////////////////////////////////////////////////////////////////// 
   
  // No input data, no commands, process analog data
  if (!ble_connected())
    queryDone = false; // reset query state
    
  if (queryDone) // only report data after the query state
  { 
    byte input_data_pending = reportDigitalInput();  
    if (input_data_pending)
    {
      ble_do_events();
      buf_len = 0;
      
      return; // only do this task in this loop
    }
  
    reportPinAnalogData();
    
    ble_do_events();
    buf_len = 0;
    
    return;  
  }
    
  ble_do_events();
  buf_len = 0;
}


 
////////////////////////////////////////////////////////////////////////////////

